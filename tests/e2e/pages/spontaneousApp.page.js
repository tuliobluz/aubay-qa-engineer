let spontaneousData = require('../data/spontaneousData.js');

let SpontaneousAppPage = function () {
    let EC = protractor.ExpectedConditions,
        nameField = element.all(by.id('Nome')),
        mobileListField = element(by.id('TelemovelList')),
        mobileField = element.all(by.id('Telemovel')),
        emailField = element.all(by.id('Email')),
        levelListField = element.all(by.id('NivelProfissionalId')),
        technologies = element(by.cssContainingText('option', 'Other')),
        linkedinField = element.all(by.id('Linkedin')),
        authorizeCheck = element.all(by.id('AutorizeRecrut')),
        authorizeCommunicationCheck = element.all(by.id('AutorizeJobOpp')),
        authorizePolicy = element.all(by.id('AutorizePolicy')),
        privacyPolicy = element.all(by.css('.personalDataProcessing p a')),
        anchorRecaptcha = element.all(by.css('.rc-anchor-light')),
        submitButton = element.all(by.id('Save')),
        msgSuccessfully = element(by.css('.toast-success')),
        msgFailure = element(by.id('toast-container')),
        titledPrivacyPolicy = element(by.css('.m-5-auto h2'));

    this.getSpontaneousEn = async function () {
        await browser.get(spontaneousData.routes.spontaneousApp);
    };

    this.fillsName = async function (name) {
        await nameField.get(0).sendKeys(name);
    }

    this.fillsMobile = async function (mobile) {
        await mobileField.get(0).sendKeys(mobile);
    }

    this.fillsEmail = async function (email) {
        await emailField.get(0).sendKeys(email);
    }

    this.fillsLevel = async function (level) {
        await levelListField.get(0).sendKeys(level);
    }

    this.clickTechnologies = async function () {
        await technologies.click();
    }

    this.fillsLinkedin = async function (linkedin) {
        await linkedinField.get(0).sendKeys(linkedin);
    }

    this.checkAuthorizationRec = async function () {
        await authorizeCheck.get(0).click();
    }

    this.checkAuthorizationCom = async function () {
        await authorizeCommunicationCheck.get(0).click();
    }

    this.checkAuthorizationPol = async function () {
        await authorizePolicy.get(0).click();
    }

    this.clickPrivacyPolicy = async function () {
        await browser.actions().mouseMove(privacyPolicy.get(0)).click().perform();
    }

    this.clickRecaptcha = async function () {
        // The Recaptcha is a Iframe so, the line below is change to his Iframe
        await browser.switchTo().frame(0);

        await anchorRecaptcha.get(0).click();

        // The line below is to return to default content
        await browser.switchTo().defaultContent();
    }

    this.clickSubmit = async function () {
        await submitButton.get(0).click();
    }

    this.getMsgSuccess = function () {
        browser.wait(EC.elementToBeClickable((msgSuccessfully)), 5000);
        return msgSuccessfully.getText();
    }

    this.getMsgFailure = function () {
        browser.wait(EC.elementToBeClickable((msgFailure)), 5000);
        return msgFailure.getText();
    }

    this.getTitledPrivacy = function () {
        browser.wait(EC.textToBePresentInElement((titledPrivacyPolicy), spontaneousData.texts.titledPrivacy), 5000);
        return titledPrivacyPolicy.getText();
    }
};
module.exports = new SpontaneousAppPage();