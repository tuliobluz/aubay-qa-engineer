'use strict';

module.exports = {
    routes: {
        "spontaneousApp": "Home/SpontaneausApp?culture=en-us",
    },
    candidate: {
        "name": "Teste QA",
        "mobileValid": "333333333",
        "mobileInvalid": "33333333",
        "emailValid": "teste@teste.com",
        "emailInvalid": "testeteste.com",
        "level": "Pleno",
        "linkedinValid": "https://www.linkedin.com/in/testelinkedin",
        "linkedinInvalid": "teste"

    },
    messages: {
        "sucess": "Thanks for the interest. We swill now work to propose a personal interview.",
        "requiredFields": "Required fields: Name Phone Email Technologies Candidate integration Privacy Policy Contact Authorization !\nSelect the Captcha box to continue",
        "emailInvalid": "Invalid email.\nSelect the Captcha box to continue",
        "mobileInvalid": "Invalid mobile number. Check the code and the number.\nSelect the Captcha box to continue",
        "linkedinInvalid": "Invalid LinkedIn.\nSelect the Captcha box to continue",
    },
    texts: {
        "titledPrivacy": "PRIVACY POLICY",
    }
};