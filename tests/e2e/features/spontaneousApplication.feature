@spontaneousApplication @e2e

Feature:  Spontaneous Application
    In order to send my application job to Aubay
    As a candidate
    I want to be able to send an application in the form

    @skippedRecaptcha @pending
    Scenario: Spontaneous Application successfully
        Given The user is in Spontaneous Application
        When The user fills the fields to the application
        And The user submits the form
        Then  The user should see the successful message

    Scenario:  Spontaneous Application required fields
        Given  The user is in Spontaneous Application
        When  The user submits the form
        Then  The user should see the required fields message

    Scenario: Spontaneous Application invalid email
        Given  The user is in Spontaneous Application
        When  The user fills the email field invalid
        And  The user submits the form
        Then  The user should see the email invalid message

    Scenario:  Spontaneous Application invalid mobile
        Given  The user is in Spontaneous Application
        When  The user fills the mobile field invalid
        And  The user submits the form
        Then  The user should see the mobile invalid message

    Scenario:  Spontaneous Application invalid linkedin
        Given  The user is in Spontaneous Application
        When  The user fills the linkedin field invalid
        And  The user submits the form
        Then  The user should see the linkedin invalid message

    Scenario:  Spontaneous Application privacy policy
        Given  The user is in Spontaneous Application
        When  The user goes to the privacy policy
        Then  The user should see the privacy policy