let spontaneousAppPage = require('../pages/spontaneousApp.page.js');
let spontaneousData = require('../data/spontaneousData.js');

let chai = require('chai');
let chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
let expect = chai.expect;
var { setDefaultTimeout } = require('cucumber');
setDefaultTimeout(60 * 1200);

Given('The user is in Spontaneous Application', async function () {
    await browser.waitForAngularEnabled(false);
    await spontaneousAppPage.getSpontaneousEn();
});

When('The user fills the fields to the application', async function () {
    await spontaneousAppPage.fillsName(spontaneousData.candidate.name);
    await spontaneousAppPage.fillsEmail(spontaneousData.candidate.emailValid);
    await spontaneousAppPage.fillsMobile(spontaneousData.candidate.mobileValid);
    await spontaneousAppPage.fillsLevel(spontaneousData.candidate.level);
    await spontaneousAppPage.clickTechnologies();
    await spontaneousAppPage.fillsLinkedin(spontaneousData.candidate.linkedinValid);
    await spontaneousAppPage.checkAuthorizationRec();
    await spontaneousAppPage.checkAuthorizationCom();
    await spontaneousAppPage.checkAuthorizationPol();
});

When('The user submits the form', async function () {
    await spontaneousAppPage.clickRecaptcha();
    await spontaneousAppPage.clickSubmit();
});

Then('The user should see the successful message', async function () {
    // expect(await spontaneousAppPage.getMsgSuccess())
    //     .to.equal(spontaneousData.messages.sucess);
    return 'pending'; //It is waiting for the disable recaptcha field
});

Then('The user should see the required fields message', async function () {
    expect(await spontaneousAppPage.getMsgFailure())
        .to.equal(spontaneousData.messages.requiredFields);
});

When('The user fills the email field invalid', async function () {
    await spontaneousAppPage.fillsName(spontaneousData.candidate.name);
    await spontaneousAppPage.fillsEmail(spontaneousData.candidate.emailInvalid);
    await spontaneousAppPage.fillsMobile(spontaneousData.candidate.mobileValid);
    await spontaneousAppPage.fillsLevel(spontaneousData.candidate.level);
    await spontaneousAppPage.clickTechnologies();
    await spontaneousAppPage.fillsLinkedin(spontaneousData.candidate.linkedinValid);
    await spontaneousAppPage.checkAuthorizationRec();
    await spontaneousAppPage.checkAuthorizationCom();
    await spontaneousAppPage.checkAuthorizationPol();
});

Then('The user should see the email invalid message', async function () {
    expect(await spontaneousAppPage.getMsgFailure())
        .to.equal(spontaneousData.messages.emailInvalid);
});

When('The user fills the mobile field invalid', async function () {
    await spontaneousAppPage.fillsName(spontaneousData.candidate.name);
    await spontaneousAppPage.fillsEmail(spontaneousData.candidate.emailValid);
    await spontaneousAppPage.fillsMobile(spontaneousData.candidate.mobileInvalid);
    await spontaneousAppPage.fillsLevel(spontaneousData.candidate.level);
    await spontaneousAppPage.clickTechnologies();
    await spontaneousAppPage.fillsLinkedin(spontaneousData.candidate.linkedinValid);
    await spontaneousAppPage.checkAuthorizationRec();
    await spontaneousAppPage.checkAuthorizationCom();
    await spontaneousAppPage.checkAuthorizationPol();
});

Then('The user should see the mobile invalid message', async function () {
    expect(await spontaneousAppPage.getMsgFailure())
        .to.equal(spontaneousData.messages.mobileInvalid);
});

When('The user fills the linkedin field invalid', async function () {
    await spontaneousAppPage.fillsName(spontaneousData.candidate.name);
    await spontaneousAppPage.fillsEmail(spontaneousData.candidate.emailValid);
    await spontaneousAppPage.fillsMobile(spontaneousData.candidate.mobileValid);
    await spontaneousAppPage.fillsLevel(spontaneousData.candidate.level);
    await spontaneousAppPage.clickTechnologies();
    await spontaneousAppPage.fillsLinkedin(spontaneousData.candidate.linkedinInvalid);
    await spontaneousAppPage.checkAuthorizationRec();
    await spontaneousAppPage.checkAuthorizationCom();
    await spontaneousAppPage.checkAuthorizationPol();
});

Then('The user should see the linkedin invalid message', async function () {
    expect(await spontaneousAppPage.getMsgFailure())
        .to.equal(spontaneousData.messages.linkedinInvalid);
});

When('The user goes to the privacy policy', async function () {
    await spontaneousAppPage.clickPrivacyPolicy();
});

Then('The user should see the privacy policy', async function () {
    expect(await spontaneousAppPage.getTitledPrivacy())
        .to.equal(spontaneousData.texts.titledPrivacy);
});