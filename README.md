## Aubay - Item 4

[Test Planning](https://docs.google.com/document/d/1iAIxa1t6iiD9ORhgNn8opu4Tk3pI3KEVqloj7CmuKN0/edit?usp=sharing)

[Report](https://docs.google.com/spreadsheets/d/1D0qumgOgNQRCnjLsMRLykMxxRHPu89CfEmozOVFarSQ/edit?usp=sharing)

### Technologies used

I used the technologies below:

* [Protractor](https://www.protractortest.org/#/): Protractor is an end-to-end test framework for Angular and AngularJS applications. Protractor runs tests against your application running in a real browser, interacting with it as a user would;
* [Page Objects](https://www.protractortest.org/#/page-objects): Page Objects help you write cleaner tests by encapsulating information about the elements on your application page. A Page Object can be reused across multiple tests, and if the template of your application changes, you only need to update the Page Object;
* [CucumberJS](https://github.com/cucumber/cucumber-js): Cucumber is a tool for running automated tests written in plain language. Because they're written in plain language, they can be read by anyone on your team. Because they can be read by anyone, you can use them to help improve communication, collaboration and trust on your team;
* [Docker](https://www.docker.com/): Docker provides container software that is ideal for developers and teams looking to get started and experimenting with container-based applications.
### Requirements

- [Node.js](https://nodejs.org/en/download/) installed;
- Its need to have the [Java Development Kit (JDK)](https://www.oracle.com/technetwork/java/javase/downloads/index.html) installed to run the standalone Selenium Server.

### To set up

- Run ```npm install``` to install dependencies

### Folders Structures

* ```e2e ```
    * ```data ``` It is to use to manage the data this way you can reuse the data in your tests
        * ```spontenousData.js ```
    * ```features ``` Where feature files should be created
        * ```spontaneousApplication.feature ```
    * ```pages ``` Where the page object of tests should be created
        * ```spontaenousApp.page.js ```
    * ```specs ``` Where the specification of tests should be created
        * ```spontaneousApp.spec.js ```

### Running tests

- Start the Selenium Server ```webdriver-manager start```

- Run the tests ```protractor protractor.conf.js```

- Just run the tests are done protractor ```protractor protractor.conf.js --cucumberOpts.tags='~@pending'``` without pending scenarios

### Headless

If you want to run the tests in your computer and Headless, you just need to set up the following steps:


- In the file ```protractor.conf.js``` add this code to your browser

```chromeOptions: { args: ['--headless', '--disable-gpu', '--no-sandbox', '--disable-extensions', '--disable-dev-shm-usage', '--window-size=1600,1020']}```

When you use that args, the protractor it will run in headless.

### Docker - PLUS

#### Requirement

- Install [Docker](https://docs.docker.com/install/)
#### Running the tests with Docker

- Run the ```docker-compose up``` to run the tests in the container

- If the repository has some changes need to rebuild the images ```docker-compose up --build```
